# simple Makefile
#
# This assumes that fastjet-config is in your path. If it is not, edit the relevant line below
CXX = g++
CXXFLAGS = -Wall -g -O2

FJCONFIG = fastjet-config
INCLUDE += `$(FJCONFIG) --cxxflags`
LIBRARIES  += `$(FJCONFIG) --libs --plugins`

COMMONSRC = LundJsonGenerator.cc
COMMONOBJ = LundJsonGenerator.o

PROGSRC = example.cc
PROGOBJ = example.o

all:  example 


example: example.o  $(COMMONOBJ)
	$(CXX) $(LDFLAGS) -o $@ $@.o $(COMMONOBJ) $(LIBRARIES)

clean:
	rm -vf $(COMMONOBJ) $(PROGOBJ)

realclean: clean
	rm -vf  example 

.cc.o:         $<
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@

depend:
	makedepend  $(INCLUDE) -Y --   -- $(COMMONSRC) $(PROGSRC)

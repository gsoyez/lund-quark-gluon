#include "LundJsonGenerator.hh"

#include "json.hpp"

using namespace std;
using namespace fastjet;
using nlohmann::json;

// conversion from PrimaryDeclusteringto json
void to_json(json& j, const PrimaryDeclustering& d) {
  j = json{{"pt", float(d.pt)}, {"m", float(d.m)},
           {"pt1", float(d.pt1)}, {"pt2", float(d.pt2)},
           {"delta_R", float(d.delta_R)}, {"z",float(d.z)}, 
           {"kt",float(d.kt)}, {"varphi", float(d.varphi)}};
}

// conversion from TreeDeclusteringto json
void to_json(json& j, const TreeDeclustering& d) {
  j = json{{"pt", float(d.pt)}, {"m1", float(d.m1)}, {"m2", float(d.m2)}, {"delta_R", float(d.delta_R)},
           {"kt",float(d.kt)}, {"z",float(d.z)}, {"varphi", float(d.varphi)},
           {"idxhard",int(d.next_hard)},{"idxsoft",int(d.next_soft)}};
}

//------------------------------------------------------------------------
/// class LundJsonGenerator implementation
//------------------------------------------------------------------------

// get the primary declusterings for a given jet
vector<PrimaryDeclustering> LundJsonGenerator::primary_declusterings(const PseudoJet &jet) const{
  vector<PrimaryDeclustering> result;
  PseudoJet jj, j1, j2;
  jj = _jet_ca_recluster(jet);
  while (jj.has_parents(j1,j2)) {
    PrimaryDeclustering declust;
    // make sure j1 is always harder branch
    if (j1.pt2() < j2.pt2()) swap(j1,j2);
    
    // store the subjets themselves
    declust.jj   = jj;
    declust.j1   = j1;
    declust.j2   = j2;
    
    // get info about the jet 
    declust.pt   = jj.pt();
    declust.m    = jj.m();
    
    // collect info about the declustering
    declust.pt1     = j1.pt();
    declust.pt2     = j2.pt();
    declust.delta_R = j1.delta_R(j2);
    declust.z       = declust.pt2 / (declust.pt1 + declust.pt2);
    declust.kt      = j2.pt() * declust.delta_R;
    
    // this is now phi along the jet axis, defined in a
    // long. boost. inv. way
    declust.varphi = atan2(j1.rap()-j2.rap(), j1.delta_phi_to(j2));
    
    // add it to our result
    result.push_back(declust);
    
    // follow harder branch
    jj = j1;
  }
  return result;
}

// get the full-tree declusterings for a given jet
vector<TreeDeclustering> LundJsonGenerator::tree_declusterings(const PseudoJet &jet) const{
  // we recluster the jet with C/A and browse the clustering history
  // backwards so that the first entry would correspond to the first
  // declustering
  //
  // As we progress through the cluster sequence history, we
  // maintain a map of which history element correspon to which
  // vertex
  //
  // Notes:
  //
  // - the last history element (first in backwards parsing)
  //   corresponds to the recombination w the beam. It has
  //   child==Invalid (-3) and parent2==BeamJet (-1). We can simply
  //   discard it
  //
  // - The first steps in history just "declare" particles i.e. have
  //   (parent1 and parent2==InexistentParent(-2)). These can also
  //   be discarded
  //
  // - for now, we order things so that hard==parent1,
  //   soft==parent2. We'll fix this in a later pass
  PseudoJet j = _jet_ca_recluster(jet);
  
  const auto & history = j.validated_cs()->history();
  const auto & jets    = j.validated_cs()->jets();

  vector<TreeDeclustering> result;
  vector<int> history2declust(history.size(),-1);
  int history_index = history.size()-1;  // the -1 skips the recombinaton w the beam
  do {
    --history_index;
    auto & elm = history[history_index];

    // stop if this just declares a particle
    if (elm.parent1 == ClusterSequence::InexistentParent) break;

    // here we have genuine branchings
    int child   = elm.child;
    int parent1 = elm.parent1;
    int parent2 = elm.parent2;
      
    // we first need the PseudoJets associated with the vertex
    const PseudoJet & j  = jets[elm.jetp_index];
    const PseudoJet & j1 = jets[history[parent1].jetp_index];
    const PseudoJet & j2 = jets[history[parent2].jetp_index];

    // compute the Lund variables
    TreeDeclustering declust;
    double pt1 = j1.pt();
    double pt2 = j2.pt();

    declust.pt      = j.pt();
    declust.m1      = j1.m();
    declust.m2      = j2.m();
    declust.delta_R = j1.delta_R(j2);
    declust.kt      = min(pt1, pt2) * declust.delta_R;
    declust.z       = pt2 / (pt1 + pt2);
    declust.varphi  = atan2(j1.rap()-j2.rap(), j1.delta_phi_to(j2));
    declust.next_hard = declust.next_soft = -1;

    // update earlier branchings to let them know where to find
    // their next branchings
    if (result.size()>0){
      int parent_vertex = history2declust[child];
      if (history[child].parent1 == history_index){
        result[parent_vertex].next_hard = result.size();
      } else {
        assert(history[child].parent2 == history_index);
        result[parent_vertex].next_soft = result.size();
      }
    }

    // update the mapping so that when we create the declusterings
    // associated w the offsprings of the current vertex, we know
    // where to update things
    history2declust[history_index] = result.size();
      
    // record new declustering
    result.push_back(declust);
  } while (history_index>0);

  // make sure things are ordered so that z<1/2 (not strictly needed
  // but would allow us to use the same splitting with z<1/2 as for
  // the primary declusterings)
  for (auto & d : result){
    if (d.z>0.5){
      // flip x, phi and the hard/soft assignment
      d.z = 1-d.z;
      double new_phi = d.varphi + M_PI;
      if (new_phi>M_PI) new_phi-= (2*M_PI);
      d.varphi = new_phi;
      std::swap(d.next_hard, d.next_soft);
    }
  }

  return result;
}

// appemd primary declusterings for "jet" to "output stream in json format
void LundJsonGenerator::append_primary(const PseudoJet &jet, ostream &output_stream) const{
  append_to_stream(primary_declusterings(jet), output_stream);
}

// appemd tree declusterings for "jet" to "output stream" in json formet
void LundJsonGenerator::append_tree(const PseudoJet &jet, ostream &output_stream) const{
  append_to_stream(tree_declusterings(jet), output_stream);
}

// appemd a series of primary declusterings to "output stream in json format
void LundJsonGenerator::append_to_stream(const vector<PrimaryDeclustering> &declusterings, ostream &output_stream) const{
  json J = declusterings;
  output_stream << J << endl;
}

// appemd a series of tree declusterings to "output stream in json format
void LundJsonGenerator::append_to_stream(const vector<TreeDeclustering> &declusterings, ostream &output_stream) const{
  json J = declusterings;
  output_stream << J << endl;
}


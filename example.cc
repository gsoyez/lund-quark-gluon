//----------------------------------------------------------------------
/// Simple example code that prints the json primary and tree
/// declusterings for the hardest jet in the given event
///
/// run it with
///   ./example < .../single-event.dat
/// with .../single-event.dat shipped with your FastJet sources

#include <iostream>
#include <fastjet/JetDefinition.hh>
#include "LundJsonGenerator.hh"

using namespace std;
using namespace fastjet;

/// an example program showing how to use fastjet
int main(){
  //----------------------------------------------------------
  // read in input particles
  vector<PseudoJet> input_particles;
  
  double px, py , pz, E;
  while (cin >> px >> py >> pz >> E) {
    input_particles.push_back(PseudoJet(px,py,pz,E)); 
  }

  //----------------------------------------------------------
  // get the hardest (anti-kt, R=0.4) jet in the event
  JetDefinition jet_def(antikt_algorithm, 0.4);
  PseudoJet jet = jet_def(input_particles)[0];

  //----------------------------------------------------------
  // create a tool to generate Json declustering output 
  LundJsonGenerator lund_json_generator;
  
  //----------------------------------------------------------
  // print the primary declusterings
  cout << "primary declusterings:" << endl;
  lund_json_generator.append_primary(jet, cout);
  cout << endl;
  
  //----------------------------------------------------------
  // print the full-tree declusterings
  cout << "tree declusterings:" << endl;
  lund_json_generator.append_tree(jet, cout);
  cout << endl;
  
  return 0;
}

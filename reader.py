#import ujson as json
import json
import gzip
import sys

#----------------------------------------------------------------------
#
# class to read json Lund ntuple files
class Reader(object):
    """
    Reader for files consisting of a sequence of json objects.
    Any pure string object is considered to be part of a header (even if it appears at the end!)
    """
    def __init__(self, infile, nmax = -1):
        self.infile = infile
        self.nmax = nmax
        self.reset()
        
    def reset(self):
        """
        Reset the reader to the start of the file, clear the header and event count.
        """
        # automatic gzip support
        if self.infile.endswith(".gz"):
            self.is_json = self.infile[:-3].endswith(".json")
            self.stream = gzip.open(self.infile,'r')
            print (f"Opening gzipped file (json is {self.is_json})", file=sys.stderr)
        else:
            self.is_json = self.infile.endswith(".json")
            self.stream = open(self.infile,'r')
            print (f"Opening uncompressed file (json is {self.is_json})", file=sys.stderr)
        self.n = 0
        self.header = []
        
        
    #----------------------------------------------------------------------
    def __iter__(self):
        # needed for iteration to work 
        return self
        
    def __next__(self):
        ev = self.next_event()
        if (ev is None): raise StopIteration
        else           : return ev

    def next(self): return self.__next__()
        
    def next_event(self):

        # we have hit the maximum number of events
        if (self.n == self.nmax):
            print ("# Exiting after having read nmax jet declusterings")
            return None
        
        try:
            if self.is_json:
                line = self.stream.readline()
                if isinstance(line,str):
                    j = json.loads(line)
                else:
                    j = json.loads(line.decode('utf-8'))
            else:
                have_non_comment = False
                while not have_non_comment:
                    line = self.stream.readline()
                    if not line.startswith("#"):
                        have_non_comment = True
                        j=float(line)
        except IOError:
            print("# got to end with IOError (maybe gzip structure broken?) around event", self.n, file=sys.stderr)
            return None
        except EOFError:
            print("# got to end with EOFError (maybe gzip structure broken?) around event", self.n, file=sys.stderr)
            return None
        except ValueError:
            print("# got to end with ValueError (empty json entry) around event", self.n, file=sys.stderr)
            return None
            
        # skip this
        if (type(j) is str):
            self.header.append(j)
            return self.next_event()
        self.n += 1
        return j


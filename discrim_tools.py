import newhistogram as nh
import sys

#----------------------------------------------------------------------
# basic class for a discriminant
#
# Not of a specific use but helps guding what user-defined
# discriminants should implement
class DiscriminatorBase(object):
    def __str__(self):
        return "# basic discriminant"
        
    def discriminant(self, event):
        return 0.0

#----------------------------------------------------------------------
# Based on a reader and a discriminator, test the performance using a
# given number of events
# 
class DiscriminantTester(object):
    """
    Class to help test a discriminant. You create one with a signal stream, 
    a second one with a background stream. And then use the ROC 
    facility to compare the results
    """
    def __init__(self, name, discrim, reader, nev, sort=True):
        """Create a Discriminant tester based on a discriminant
        (discrim), using nev events from the reader for the test. For
        the purpose of histograms, etc., assign name to the output.
        
        To get the final output, one needs to run two discriminant
        testers, one for the sigal, one for the background.

        """
        self.array = []
        self.name  = name

        if discrim is not None:
            self.hist = nh.Histogram(nh.LinearBins(discrim.binmin(),
                                                   discrim.binmax(),
                                                   discrim.binsize()),
                                     name='DT-'+name)
        else:
            self.hist = nh.Histogram(nh.LinearBins(-20.0, 10.0, 0.1),
                                     name='DT-'+name)
        print ('processing ',name,file=sys.stderr)
        iev_print = 1000
        for i in range(nev):
            event = reader.next()
            d = discrim.discriminant(event) if discrim is not None else event
            self.array.append(d)
            self.hist.add(d)
            if (i+1)%iev_print == 0:
                print (f"  done {i+1} events", file=sys.stderr)
                if (i+1) == 15*iev_print:
                    iev_print*=10
            
        # it will be useful when creating the ROC curve later to have
        # this array sorted
        if sort:
            self.array.sort()

    def __str__(self):
        return "# Discriminant tester content ({})\n{}".format(self.name, self.hist.__str__())
        
    def ROC_v_bkgd(self, bkgd):
        """
        produce the ROC curve, assuming self is the signal, while bkgd is a separately
        created background discriminant tester
        """

        # convenient alises
        array_s = self.array
        array_b = bkgd.array

        result = []
        i_s = len(array_s) - 1
        i_b = len(array_b) - 1
        while i_s >= 0:
            # find background position that corresponds to a
            # discriminant that is just below the current value of the
            # signal discriminant
            while (i_b >= 0 and array_b[i_b] > array_s[i_s]): i_b -= 1
            # exit the loop if the searh was unsuccessful
            if (array_b[i_b] > array_s[i_s]): break

            # store the result
            result.append([1-(1.0 + i_s)/len(array_s), 1-(1.0 + i_b)/len(array_b), array_s[i_s]])
            i_s -= int(len(array_s)/100)

        return result

    def ROC_v_bkgd_new(self, bkgd, targetted_droc=0.002):
        """
        produce the ROC curve, assuming self is the signal, while bkgd is a separately
        created background discriminant tester

        Note : things changed compared to our older case because we
        assume that the quark case has smaller values than the gluon
        case (so we browse the vectors in the opposite ditrection)
        """

        # convenient alises
        array_s = self.array
        array_b = bkgd.array

        result = []
        i_s = 0
        i_b = 0
        last_i = 0
        di_min = int(len(array_s) * targetted_droc)
        while i_s < len(array_s):
            # get a threshold that is halfway between two signal bins,
            # requiring the two signal bins to have different
            # values of the likelihood
            while (array_s[i_s+1] == array_s[i_s] and i_s < len(array_s)-2): i_s += 1
            # we have failed to find a threshold, so exit
            if (i_s == len(array_s)-2 and array_s[i_s] == array_s[i_s+1]): break

            threshold = 0.5 * (array_s[i_s] + array_s[i_s+1])
            #threshold = array_s[i_s]
            
            # find background position that corresponds to a
            # discriminant that is just below the current value of the
            # signal discriminant
            while (i_b < len(array_b) and array_b[i_b] < threshold): i_b += 1

            # exit the loop if the search was unsuccessful
            if i_b == len(array_b): break
            if (array_b[i_b] < array_s[i_s]): break

            # store the result
            if (i_s >= last_i + di_min):
                result.append([i_s/len(array_s), i_b/len(array_b), array_s[i_s]])
                last_i = i_s

            if (i_s == len(array_s)-2): break
            i_s += 1

        return result




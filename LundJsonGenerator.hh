#ifndef __LUND_JSON_GENERATOR_HH__
#define __LUND_JSON_GENERATOR_HH__

#include <fstream>
#include <fastjet/PseudoJet.hh>
#include "fastjet/tools/Recluster.hh"

//------------------------------------------------------------------------
/// structure for primary declusterings
//------------------------------------------------------------------------
struct PrimaryDeclustering {
  // the (sub)jet, and its two declustering parts, for subsequent use
  fastjet::PseudoJet jj, j1, j2;
  // variables of the (sub)jet about to be declustered
  double pt, m;
  // properties of the declustering; NB kt is the _relative_ kt
  // = pt2 * delta_R (ordering is pt2 < pt1)
  double pt1, pt2, delta_R, z, kt, varphi;
};

//------------------------------------------------------------------------
/// structure for the full-tree declusterings
//------------------------------------------------------------------------
struct TreeDeclustering {
  // variables of the (sub)jet about to be declustered
  double pt, m1, m2;
  // properties of the declustering; NB kt is the _relative_ kt
  // = pt2 * delta_R (ordering is pt2 < pt1)
  double delta_R, kt, z, varphi;
  // information about where to find next declusterings on both
  // brances (-1 means no children)
  int next_hard, next_soft;
};

//------------------------------------------------------------------------
/// \class LundJsonGenerator
/// class to get the Lund declusterings from a jet and output them in json format
///
/// Calling apped_primary(PseudoJet) or append_tree(PseudoJet) will
/// compute the declusterings for the given jet and add them to the
/// output file.
class LundJsonGenerator{
public:
  /// ctor taking an output stream as argument
  LundJsonGenerator()
    : _jet_ca_recluster(fastjet::cambridge_aachen_algorithm){}

  /// get the primary declusterings for a given jet
  std::vector<PrimaryDeclustering> primary_declusterings(const fastjet::PseudoJet &jet) const; 
  
  /// get the full-tree declusterings for a given jet
  std::vector<TreeDeclustering> tree_declusterings(const fastjet::PseudoJet &jet) const; 
  
  /// appemd primary declusterings for "jet" to "output stream in json format
  void append_primary(const fastjet::PseudoJet &jet,
                      std::ostream &output_stream) const;

  /// appemd tree declusterings for "jet" to "output stream" in json formet
  void append_tree(const fastjet::PseudoJet &jet,
                   std::ostream &output_stream) const;

  /// appemd a series of primary declusterings to "output stream in json format
  void append_to_stream(const std::vector<PrimaryDeclustering> &declusterings,
                        std::ostream &output_stream) const;

  /// appemd a series of tree declusterings to "output stream in json format
  void append_to_stream(const std::vector<TreeDeclustering> &declusterings,
                        std::ostream &output_stream) const;

protected:
  fastjet::Recluster _jet_ca_recluster;
};

#endif  // __LUND_JSON_GENERATOR_HH__


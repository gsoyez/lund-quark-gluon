This folder contains code snippets implementing the analytic
quark/gluon discriminant based on Lund-plane declusterings.


It basically consists of two parts:


1. a LundJsonGenerator class used to generate JSON declustering files
   which can then be processed for quark/gluon discrimination

This is implemented in the LundJsonGenerator.{hh,cc} files. An
"example.cc" file is provided to illustrate how the class works. This
can all be compiled using the Makefile in this folder.


2. a likelihood-analytic.py python script that reads either one or two
   JSON files and computes the quark/gluon discrimunation. See the
   text at the top of the file for details on how to use it.  (the
   discrim_tools.py, newhistogram.py and reader.py scripts contain
   tools used internally in the main python script)


If you use this code, please cite:

  Quarks and gluon in the Lund plane,
  Frederic A. Dreyer, Gregory Soyez and Adam Takacs
  arXiv:2112.09140


Code for the Lund-Net neural network implementation can be found at:

  https://github.com/fdreyer/lundnet


